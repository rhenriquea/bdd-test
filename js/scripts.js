$(document).ready(function(){
    //data-last-move attribute is setted to "left"
    var lastMove = $('body').data('last-move');

    var img = $('.dancing');
    img.click(function(){
        nextMove();
    });
    
    function nextMove(){
        if (lastMove == "left"){
            moveRight();
        } else {
            moveLeft();
        }
    }
    
    function moveRight(){
        img.animate({ "left": "+=30px" }, "slow" );
        moveLeft();
        $('body').data('last-move','right');
    }
    
    function moveLeft(){
        img.animate({ "left": "-=30px" }, "slow" );
        moveRight();
        $('body').data('last-move','left');
    }
});





